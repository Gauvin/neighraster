---
title: "montRoyalParliamentHill"
author: "Charles"
date: "September 6, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

 
# Librairies
 
```{r, include=FALSE}

GeneralHelpers::loadAll()
library(tidyverse)
library(magrittr)
library(ggplot2)
 
 
library(sf)
 
library(here)
library(glue)

#Custom packages
library(Census2SfSp)
library(neighShp) 

```
 
#Qc
```{r pressure, echo=FALSE}
rastQcFirst <- raster(here::here("Data","GeoData","Raster", "cdem_dem_021L.tif"))
rastQcSecond <-  raster(here::here("Data","GeoData","Raster", "cdem_dem_021M.tif"))
 
rastMergedQc <- merge(rastQcFirst, rastQcSecond)

rastQcFileName<- here::here("Data","GeoData","Raster", "QcIlesMerged") 
writeRaster(rastMergedQc,
            rastQcFileName,
            overwrite=T
            )

```

```{r}

rastMergedQc <- raster(rastQcFileName)
rastMergedQc %>% plot

```

 
#Mtl
```{r pressure, echo=FALSE}
rastMtlFirst <- raster(here::here("Data","GeoData","Raster", "cdem_dem_031H.tif"))
rastMtlSecond <-  raster(here::here("Data","GeoData","Raster", "cdem_dem_031I.tif"))
 
rastMergedMtl <- merge(rastMtlFirst, rastMtlSecond)

rastFileNameMtl <- here::here("Data","GeoData","Raster", "MtlHillsMonteregieMerged.tif")
writeRaster(rastMergedMtl,
             rastFileNameMtl
            )

 
```


```{r}

rastMergedMtl <- raster(rastFileNameMtl)
rastMergedMtl %>% plot

```
